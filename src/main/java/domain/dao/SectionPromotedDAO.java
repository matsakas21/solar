package domain.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import domain.dto.SectionPromotedDTO;
import domain.generic.GenericHibernateDAO;

public class SectionPromotedDAO extends GenericHibernateDAO<SectionPromotedDTO, Long>{

	
	public List<SectionPromotedDTO> findPromoted(Long entry_id)
	  {
	    Criteria crit = getSession().createCriteria(getPersistentClass());
	   
	    crit.setMaxResults(1);
	    crit.add(Restrictions.eq("entry_id", entry_id));
	    List<SectionPromotedDTO> results =  crit.list();
	    return results;
	    
	  }
}
