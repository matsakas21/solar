package domain.dao;

import domain.dto.SolarGridRequestDTO;
import domain.dto.ThermalRequestDTO;
import domain.generic.GenericHibernateDAO;

public class ThermalRequestDAO extends GenericHibernateDAO<ThermalRequestDTO, Long>{

}
