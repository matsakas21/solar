package domain.dao;

import domain.dto.RequestDTO;
import domain.generic.GenericHibernateDAO;

public class RequestDAO extends GenericHibernateDAO<RequestDTO, Long>
{
}