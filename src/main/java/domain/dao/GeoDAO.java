package domain.dao;

import domain.dto.GeoDTO;
import domain.generic.GenericHibernateDAO;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

public class GeoDAO extends GenericHibernateDAO<GeoDTO, Long>
{
  public List<GeoDTO> findNearBy(Double latitude, Double longitude)
  {
    Criteria crit = getSession().createCriteria(getPersistentClass());
    
    Double minLat = latitude - 10.0;
    Double maxLat = latitude + 10.0;
    Double minLong = longitude - 10.0;
    Double maxLong = longitude + 10.0;
    System.out.println(minLat + " " + maxLat + " " + minLong + " " + maxLong);
    crit.add(Restrictions.between("latitude", minLat, maxLat)).add(Restrictions.between("longitude", minLong, maxLong));
    crit.setMaxResults(10);
    crit.add(Restrictions.eq("fid", Long.valueOf("113")));
    List<GeoDTO> results =  crit.list();
    return results;
    
  }
}