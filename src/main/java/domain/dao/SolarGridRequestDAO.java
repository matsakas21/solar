package domain.dao;

import domain.dto.SolarGridRequestDTO;
import domain.generic.GenericHibernateDAO;

public class SolarGridRequestDAO extends GenericHibernateDAO<SolarGridRequestDTO, Long>
{
}