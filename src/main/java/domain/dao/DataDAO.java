package domain.dao;

import domain.dto.DataDTO;
import domain.generic.GenericHibernateDAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

public class DataDAO extends GenericHibernateDAO<DataDTO, Long>
{
  public List<DataDTO> findDataForPlace(Long sid)
  {
    List<Criterion> criteria = new ArrayList<Criterion>();

    if (sid != null)
    {
      criteria.add(Restrictions.eq("sid", sid));
    }
//    criteria.add(Restrictions.eq("approved", Integer.valueOf("1")));
    criteria.add(Restrictions.eq("enabled", Integer.valueOf("1")));
//    criteria.add(Restrictions.eq("section", Integer.valueOf("1")));

    return findByCriteria(criteria);
  }
}