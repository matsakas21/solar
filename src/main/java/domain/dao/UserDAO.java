package domain.dao;

import domain.dto.UserDTO;
import domain.generic.GenericHibernateDAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

public class UserDAO extends GenericHibernateDAO<UserDTO, Long>
{
  public List<UserDTO> findByName(String name, String surname)
  {
    List<Criterion> criteria = new ArrayList<Criterion>();

    if (name != null)
    {
      criteria.add(Restrictions.eq("name", name));
    }

    if (surname != null)
    {
      criteria.add(Restrictions.eq("surname", surname));
    }

    return findByCriteria(criteria);
  }

  public List<UserDTO> findByUsernamePassword(String name, String email)
  {
    List<Criterion> criteria = new ArrayList<Criterion>();

    if (name != null)
    {
      criteria.add(Restrictions.eq("username", name));
    }

    if (email != null)
    {
      criteria.add(Restrictions.eq("email", email));
    }

    return findByCriteria(criteria);
  }

  public UserDTO findByUniqueFacebookID(String facebookID)
  {
    List<Criterion> criteria = new ArrayList<Criterion>();

    if (facebookID != null)
    {
      criteria.add(Restrictions.eq("facebookID", facebookID));
    }

    return (UserDTO)findUniqueResult(criteria);
  }
}