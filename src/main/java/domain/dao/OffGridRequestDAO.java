package domain.dao;

import domain.dto.OffGridRequestDTO;
import domain.generic.GenericHibernateDAO;

public class OffGridRequestDAO extends GenericHibernateDAO<OffGridRequestDTO, Long>
{
}