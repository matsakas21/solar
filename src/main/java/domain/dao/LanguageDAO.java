package domain.dao;

import domain.dto.LanguageDTO;
import domain.dto.UserDTO;
import domain.generic.GenericHibernateDAO;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

public class LanguageDAO extends GenericHibernateDAO<LanguageDTO, Long>
{
	public List<LanguageDTO> findBySKey(String sKey)
	  {
		List<Criterion> criteria = new ArrayList<Criterion>();
System.out.println("dao " + sKey);
	    if (sKey != null)
	    {
	      criteria.add(Restrictions.eq("sKey", sKey));
	    }
	    System.out.println(criteria.toString());
//	    List<LanguageDTO> lans = crit.list();
//	    for(LanguageDTO lan : lans){
//	    	System.out.println("dao " + lan.getsKey());
//	    }
	    
	    return findByCriteria(criteria);
	  }
}
