package domain.dao;

import domain.dto.OffGridRequestDTO;
import domain.dto.WindRequestDTO;
import domain.generic.GenericHibernateDAO;

public class WindRequestDAO extends GenericHibernateDAO<WindRequestDTO, Long>
{
}