package domain.dto;

import java.io.Serializable;
import java.sql.Date;

public class UserDTO
  implements Serializable
{
  private Long id;
  private String name;
  private String username;
  private String usertype;
  private String email;
  private String password;
  private Date registerDate;
  private Date lastvisitDate;

  public Long getId()
  {
    return this.id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  public String getName() {
    return this.name;
  }
  public void setName(String name) {
    this.name = name;
  }
  public String getUsername() {
    return this.username;
  }
  public void setUsername(String username) {
    this.username = username;
  }
  public String getUsertype() {
    return this.usertype;
  }
  public void setUsertype(String usertype) {
    this.usertype = usertype;
  }
  public String getEmail() {
    return this.email;
  }
  public void setEmail(String email) {
    this.email = email;
  }
  public String getPassword() {
    return this.password;
  }
  public void setPassword(String password) {
    this.password = password;
  }
  public Date getRegisterDate() {
    return this.registerDate;
  }
  public void setRegisterDate(Date registerDate) {
    this.registerDate = registerDate;
  }
  public Date getLastvisitDate() {
    return this.lastvisitDate;
  }
  public void setLastvisitDate(Date lastvisitDate) {
    this.lastvisitDate = lastvisitDate;
  }
}