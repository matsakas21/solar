package domain.dto;

import java.io.Serializable;
import java.sql.Date;

public class SobiproObject implements Serializable{
	
	private Long id;
	private Date validUntil;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getValidUntil() {
		return validUntil;
	}
	public void setValidUntil(Date validUntil) {
		this.validUntil = validUntil;
	}
	
	
	

}
