package domain.dto;

import java.io.Serializable;

public class LanguageDTO implements Serializable {
	
	private String sKey;
//	private Integer fid;
	private String sValue;
	private Long id;
//	private String language;
	public String getsKey() {
		return sKey;
	}
	public void setsKey(String sKey) {
		this.sKey = sKey;
	}
//	public Integer getFid() {
//		return fid;
//	}
//	public void setFid(Integer fid) {
//		this.fid = fid;
//	}
	public String getsValue() {
		return sValue;
	}
	public void setsValue(String sValue) {
		this.sValue = sValue;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
//	public String getLanguage() {
//		return language;
//	}
//	public void setLanguage(String language) {
//		this.language = language;
//	}
	
	

}
