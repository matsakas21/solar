package domain.dto;

import java.io.Serializable;

public class UserGroupUserDTO implements Serializable
{
  private Long user_id;
  private Long group_id;

  public Long getUser_id()
  {
    return this.user_id;
  }
  public void setUser_id(Long user_id) {
    this.user_id = user_id;
  }
  public Long getGroup_id() {
    return this.group_id;
  }
  public void setGroup_id(Long group_id) {
    this.group_id = group_id;
  }
}