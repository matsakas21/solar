package domain.dto;

import java.io.Serializable;

public class SectionPromotedDTO implements Serializable{
	
	private Long id;
	private Long entry_id;
	private Long ordering;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getEntry_id() {
		return entry_id;
	}
	public void setEntry_id(Long entry_id) {
		this.entry_id = entry_id;
	}
	public Long getOrdering() {
		return ordering;
	}
	public void setOrdering(Long ordering) {
		this.ordering = ordering;
	}
	
	

}
