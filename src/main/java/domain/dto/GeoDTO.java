package domain.dto;

import java.io.Serializable;

public class GeoDTO implements Serializable
{
  private Long fid;
  private Long sid;
  private Double longitude;
  private Double latitude;

  public Long getFid()
  {
    return this.fid;
  }
  public void setFid(Long fid) {
    this.fid = fid;
  }
  public Long getSid() {
    return this.sid;
  }
  public void setSid(Long sid) {
    this.sid = sid;
  }
  public Double getLongitude() {
    return this.longitude;
  }
  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }
  public Double getLatitude() {
    return this.latitude;
  }
  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }
}