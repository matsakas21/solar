package domain.dto;

import java.io.Serializable;
import java.sql.Date;

public class RequestDTO
  implements Serializable
{
  private Long id;
  private Double longitude;
  private Double latitude;
  private Long userID;
  private Date date;

  public Long getId()
  {
    return this.id;
  }
  public void setId(Long id) {
    this.id = id;
  }
  public Double getLongitude() {
    return this.longitude;
  }
  public void setLongitude(Double longitude) {
    this.longitude = longitude;
  }
  public Double getLatitude() {
    return this.latitude;
  }
  public void setLatitude(Double latitude) {
    this.latitude = latitude;
  }
  public Long getUserID() {
    return this.userID;
  }
  public void setUserID(Long userID) {
    this.userID = userID;
  }
  public Date getDate() {
    return this.date;
  }
  public void setDate(Date date) {
    this.date = date;
  }
}