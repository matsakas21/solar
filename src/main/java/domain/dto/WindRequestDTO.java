package domain.dto;

import java.sql.Date;

public class WindRequestDTO {
	private Long cf_id;
	private String email;
	private String longitude;
	private String latitude;
	private String location;
	private String radio_location;
	private Date cf_created;
	private Long cf_user_id;
	private String description;
	private Integer vstatus;
	private String cf_ipaddress;
	private String link;
	private String firstname;
	private String Last_Name;
	public Long getCf_id() {
		return cf_id;
	}
	public void setCf_id(Long cf_id) {
		this.cf_id = cf_id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getRadio_location() {
		return radio_location;
	}
	public void setRadio_location(String radio_location) {
		this.radio_location = radio_location;
	}
	public Date getCf_created() {
		return cf_created;
	}
	public void setCf_created(Date cf_created) {
		this.cf_created = cf_created;
	}
	public Long getCf_user_id() {
		return cf_user_id;
	}
	public void setCf_user_id(Long cf_user_id) {
		this.cf_user_id = cf_user_id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Integer getVstatus() {
		return vstatus;
	}
	public void setVstatus(Integer vstatus) {
		this.vstatus = vstatus;
	}
	public String getCf_ipaddress() {
		return cf_ipaddress;
	}
	public void setCf_ipaddress(String cf_ipaddress) {
		this.cf_ipaddress = cf_ipaddress;
	}
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLast_Name() {
		return Last_Name;
	}
	public void setLast_Name(String last_Name) {
		Last_Name = last_Name;
	}
}
