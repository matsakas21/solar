package domain.dto;

import java.io.Serializable;

public class DataDTO
  implements Serializable
{
  private Long fid;
  private Long sid;
  private String basedata;
  private Integer approved;
  private Integer section;
  private Integer enabled;
  

  public Integer getSection() {
	return section;
}
public void setSection(Integer section) {
	this.section = section;
}

public Integer getEnabled() {
	return enabled;
}
public void setEnabled(Integer enabled) {
	this.enabled = enabled;
}
public Integer getApproved()
  {
    return this.approved;
  }
  public void setApproved(Integer approved) {
    this.approved = approved;
  }
  public Long getFid() {
    return this.fid;
  }
  public void setFid(Long fid) {
    this.fid = fid;
  }
  public Long getSid() {
    return this.sid;
  }
  public void setSid(Long sid) {
    this.sid = sid;
  }
  public String getBasedata() {
    return this.basedata;
  }
  public void setBasedata(String basedata) {
    this.basedata = basedata;
  }
}