package domain.generic;

import java.io.Serializable;
import java.util.List;

public abstract interface GenericDAO<T, ID extends Serializable>
{
  public abstract T findById(ID paramID, boolean paramBoolean);

  public abstract List<T> findAll();

  public abstract List<T> findByExample(T paramT, String[] paramArrayOfString);

  public abstract T makePersistent(T paramT);

  public abstract void makeTransient(T paramT);
}