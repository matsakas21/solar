package servlets;

import domain.dao.UserDAO;
import domain.dto.UserDTO;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Calendar;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.json.JSONObject;
@WebServlet("/Login")
public class Login extends HttpServlet
{
  private static final long serialVersionUID = 1L;

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    StringBuffer jb = new StringBuffer();
    String line = null;
    try {
      request.setCharacterEncoding("UTF-8");
      BufferedReader reader = request.getReader();
      while ((line = reader.readLine()) != null)
        jb.append(line);
    } catch (Exception localException1) {
    }
    try {
      System.out.println("Login");
      InitialContext ctx2 = new InitialContext();
      UserTransaction utx = (UserTransaction)ctx2.lookup("java:module/UserTransaction");
      utx.begin();
      request.setCharacterEncoding("UTF-8");
      String jsonStr = jb.toString();
      JSONObject data = new JSONObject(jsonStr);
      UserDAO userDAO = new UserDAO();
      UserDTO user = userDAO.findById(data.getLong("userID"), false);
      JSONObject jo = new JSONObject();
      if (user != null) {
        user.setLastvisitDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        user = (UserDTO)userDAO.makePersistent(user);
        jo.put("userID", user.getId());
        jo.put("response", "ok");
      } else {
        jo.put("response", "problem");
      }
      utx.commit();
      response.setCharacterEncoding("UTF-8");
      response.getOutputStream().write(jo.toString().getBytes());
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}