package servlets;

import domain.dao.DataDAO;
import domain.dao.GeoDAO;
import domain.dao.LanguageDAO;
import domain.dao.OffGridRequestDAO;
import domain.dao.RequestDAO;
import domain.dao.SectionPromotedDAO;
import domain.dao.SobiproObjectDAO;
import domain.dao.SolarGridRequestDAO;
import domain.dao.ThermalRequestDAO;
import domain.dao.UserDAO;
import domain.dao.WindRequestDAO;
import domain.dto.DataDTO;
import domain.dto.GeoDTO;
import domain.dto.LanguageDTO;
import domain.dto.OffGridRequestDTO;
import domain.dto.RequestDTO;
import domain.dto.SectionPromotedDTO;
import domain.dto.SobiproObject;
import domain.dto.SolarGridRequestDTO;
import domain.dto.ThermalRequestDTO;
import domain.dto.UserDTO;
import domain.dto.WindRequestDTO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.json.JSONArray;
import org.json.JSONObject;

public class GetNearBy extends HttpServlet
{
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException
	{
		StringBuffer jb = new StringBuffer();
		String line = null;
		try {
			request.setCharacterEncoding("UTF-8");
			BufferedReader reader = request.getReader();
			while ((line = reader.readLine()) != null)
				jb.append(line);
		} catch (Exception localException1) {
		}
		try {
			System.out.println("GetNearBy");
			InitialContext ctx2 = new InitialContext();
			UserTransaction utx = (UserTransaction)ctx2.lookup("java:module/UserTransaction");
			utx.begin();
			request.setCharacterEncoding("UTF-8");
			String jsonStr = jb.toString();
			JSONObject data = new JSONObject(jsonStr);
			Double latitude = Double.valueOf(data.getDouble("latitude"));
			Double longitude = Double.valueOf(data.getDouble("longitude"));
			if(!data.getString("type").equals("companies")){
				String location = data.getString("location");
				String radio_type = data.getString("projectType");
				String area = "";
				if(data.has("area")){
					area = data.getString("area");
				}
				
				

				Long userID = Long.valueOf(data.getLong("userID"));
				UserDAO userDAO = new UserDAO();
				UserDTO user = userDAO.findById(userID, false);

				//      String email = data.getString("email");
				if(data.getString("type").equals("solarGrid")){
					//    	  String electricity = data.getString("energy");
					SolarGridRequestDAO reqDAO = new SolarGridRequestDAO(); 
					SolarGridRequestDTO req = new SolarGridRequestDTO();
					req.setArea(area);
					req.setCf_created(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
					req.setEmail(user.getEmail());
					req.setLatitude(latitude.toString());
					req.setLongitude(longitude.toString());
					req.setLocation(location);
					req.setRadio_type(radio_type);
					req.setFirstname(user.getName());
					//    	  req.setElectricity(electricity);
					req.setCf_user_id(userID);
					req.setVstatus(1);
					req.setCf_ipaddress(data.getString("ipaddress"));
					String demand = data.getString("energyDemand");
					Double demandDouble = Double.valueOf(demand);
					Integer demandInt = demandDouble.intValue();
					String description = "Request for quotation from: " + user.getUsername() +
							"<br>The PV grid connected system will be located in <strong>" + location + 
							"</strong> and is a <strong>" + data.getString("projectType") +
							"</strong> project that is consuming annually <strong>" + demandInt + 
							" Wh</strong>. The available area for the system is <strong>" + area + " m2 </strong>.<br>";
					req.setDescription(description);
					req.setLink("project-requests/pv_grid_project?sunray=");
					System.out.println(description);
					req = reqDAO.makePersistent(req);
				}else if(data.getString("type").equals("offGrid")){
					//    	  String use_days = data.getString("days");
					OffGridRequestDAO reqDAO = new OffGridRequestDAO(); 
					OffGridRequestDTO req = new OffGridRequestDTO();
					req.setArea(area);
					req.setCf_created(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
					req.setEmail(user.getEmail());
					req.setLatitude(latitude.toString());
					req.setLongitude(longitude.toString());
					req.setLocation(location);
					req.setRadio_type(radio_type);
					req.setVstatus(1);
					req.setCf_ipaddress(data.getString("ipaddress"));
					req.setFirstname(user.getName());
					//    	  req.setUse_days(use_days);
					req.setCf_user_id(userID);
					String demand = data.getString("energyDemand");
					Double demandDouble = Double.valueOf(demand);
					Integer demandInt = demandDouble.intValue();
					String description;
					if(data.has("battery")){
						description = "Request for quotation from: " + user.getUsername() +
								"<br>The off-grid connected system will be located in <strong>" + location + 
								"</strong> and is a <strong>" + data.getString("projectType") +
								"</strong> system that will need <strong>" + demandInt +
								"Wh</strong> of energy per day with an autonomy of <strong>" + data.getString("days") +
								" days</strong>. The available area is <strong>" + area + " m2" +
								"</strong> and the storage is estimated at <strong>" + data.getString("battery") + "Ah (" + 
								data.getString("energy") + " V)</strong>. <br>";
					}else{
						description = "Request for quotation from: " + user.getUsername() +
								"<br>The off-grid connected system will be located in <strong>" + location + 
								"</strong> and is a <strong>" + data.getString("projectType") +
								"</strong> system that will need <strong>" + demandInt +
								"Wh</strong> of energy per day with an autonomy of <strong>" + data.getString("days") +
								" days</strong>. The available area is <strong>" + area + " m2" +
								"</strong>.<br>";
					}

					System.out.println(description);
					req.setLink("project-requests/off-grid_project?sunray=");
					req.setDescription(description);
					req = reqDAO.makePersistent(req);
				}else if(data.getString("type").equals("thermal")){
					ThermalRequestDAO reqDAO = new ThermalRequestDAO(); 
					ThermalRequestDTO req = new ThermalRequestDTO();
					String storage = data.getString("storage");
					String people = data.getString("people");
					req.setPeople(people);
					req.setStorage(storage);
					req.setArea(area);
					req.setCf_created(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
					req.setEmail(user.getEmail());
					req.setLatitude(latitude.toString());
					req.setLongitude(longitude.toString());
					req.setLocation(location);
					req.setRadio_type(radio_type);
					req.setCf_user_id(userID);
					req.setVstatus(1);
					req.setCf_ipaddress(data.getString("ipaddress"));
					req.setFirstname(user.getName());

					String description = "Request for quotation from: " + user.getUsername() +
							"<br>The solar thermal system will be located in <strong>" + req.getLocation() + "</strong> and is for a " +
							"<strong>" + req.getRadio_type() + "</strong> for a capacity of <strong>" + req.getPeople() + "</strong> people per " +
							"day . The estimated solar thermal panel area is <strong>" + req.getArea() + "sq.m.</strong> and the " +
							"water storage is estimated at <strong>" + req.getStorage() + "Liters</strong>.<br>";
							
					req.setDescription(description);
					req.setLink("project-requests/solar_thermal_project?sunray=");
					System.out.println(description);
					req = reqDAO.makePersistent(req);
				}else if(data.getString("type").equals("wind")){
					WindRequestDAO reqDAO = new WindRequestDAO(); 
					WindRequestDTO req = new WindRequestDTO();
					req.setCf_created(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
					req.setEmail(user.getEmail());
					req.setLatitude(latitude.toString());
					req.setLongitude(longitude.toString());
					req.setLocation(location);
					req.setRadio_location(radio_type);
					req.setCf_user_id(userID);
					req.setVstatus(1);
					req.setCf_ipaddress(data.getString("ipaddress"));
					req.setFirstname(user.getName());
					String description = "Request for quotation from: " + user.getUsername() +
							"<br>The small wind power system will be located in <strong>" + req.getLocation() + "</strong>. For a <strong>" +
							data.getString("rp") + " kW wind turbine</strong> is estimated to produce <strong>" + data.getString("output") +
							"kWh/year (cap. factor " + data.getString("cp") + "%)</strong>. <br>";
							
					req.setDescription(description);
					req.setLink("project-requests/wind_project?sunray=");
					System.out.println(description);
					req = reqDAO.makePersistent(req);
				}

			}

			JSONArray j = new JSONArray();
			GeoDAO geoDAO = new GeoDAO();
			DataDAO dataDAO = new DataDAO();
			List<GeoDTO> geos = geoDAO.findNearBy(latitude, longitude);
			SobiproObjectDAO soDAO = new SobiproObjectDAO();
			SectionPromotedDAO spDAO = new SectionPromotedDAO();
			LanguageDAO langDAO = new LanguageDAO();
			System.out.println(geos.size());
			for (int i = 0; i < geos.size(); i++) {
				
				SobiproObject so = soDAO.findById(geos.get(i).getSid(), false);
				boolean ch = so.getValidUntil() != null && so.getValidUntil().after(new Date(Calendar.getInstance().getTime().getTime()));
				System.out.println(so.getValidUntil().toString());
				System.out.println(ch);
				if(so.getValidUntil() != null && so.getValidUntil().after(new Date(Calendar.getInstance().getTime().getTime()))){
					
					List<DataDTO> fields = dataDAO.findDataForPlace(((GeoDTO)geos.get(i)).getSid());
					JSONObject jo = new JSONObject();
					jo.put("sid", String.valueOf(((GeoDTO)geos.get(i)).getSid()));
					jo.put("latitude", String.valueOf(((GeoDTO)geos.get(i)).getLatitude()));
					jo.put("longitude", String.valueOf(((GeoDTO)geos.get(i)).getLongitude()));
					boolean found = false;
					for (int k = 0; k < fields.size(); k++) {
						if(fields.get(k).getFid() != 112){
							jo.put(String.valueOf(((DataDTO)fields.get(k)).getFid()), ((DataDTO)fields.get(k)).getBasedata());
						}else{
//							System.out.println(String.valueOf(((GeoDTO)geos.get(i)).getSid()) + " " + fields.get(k).getBasedata());
//							
//							 List<LanguageDTO> language = langDAO.findBySKey(fields.get(k).getBasedata());
//							for(LanguageDTO lan : language){
//								System.out.println(lan.getsValue() +  " " + lan.getsKey());
//							}
//							System.out.println(String.valueOf(((GeoDTO)geos.get(i)).getSid()) + " " + language.toString());
//							System.out.println(String.valueOf(((GeoDTO)geos.get(i)).getSid()) + " " + fields.get(k).getBasedata() + " " + language.size());
//							if(!language.isEmpty()){
//								System.out.println(String.valueOf(((GeoDTO)geos.get(i)).getSid()) + " " + language.get(0).getsValue() + " " + language.get(0).getsKey());
//								jo.put("112", language.get(0).getsValue());
//							}else{
							
							String language = fields.get(k).getBasedata();
							String languageFinal = new String();
							if(language.length()>1){
								language = language.replace("-", " ");
								language = language.substring(0, 1).toUpperCase() + language.substring(1, language.length());
								String[] lans = language.split(" ");
								for (int h=0; h<lans.length; h++){
									System.out.println(lans[h]);
									
									languageFinal = languageFinal + " " + lans[h].substring(0, 1).toUpperCase() + lans[h].substring(1, lans[h].length());
								}
							}
							
							
//								jo.put("112", fields.get(k).getBasedata());
							jo.put("112", languageFinal);
								
//							}

						}

						if(fields.get(k).getFid() == 127){
							found = true;
						}
					}
					if(found == true){
						List<SectionPromotedDTO> sps = spDAO.findPromoted(geos.get(i).getSid());
						if(!sps.isEmpty()){
							jo.put("promoted", sps.get(0).getOrdering());
						}

						j.put(jo);
					}

				}



			}


			utx.commit();
			response.setCharacterEncoding("UTF-8");
			response.getOutputStream().write(j.toString().getBytes());
		}
		catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
}