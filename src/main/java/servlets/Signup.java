package servlets;

import domain.dao.UserDAO;
import domain.dao.UsergroupUserDAO;
import domain.dto.UserDTO;
import domain.dto.UserGroupUserDTO;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Calendar;
import java.util.List;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.UserTransaction;

import org.json.JSONObject;

@WebServlet("/Signup")
public class Signup extends HttpServlet
{
  private static final long serialVersionUID = 1L;

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException
  {
    StringBuffer jb = new StringBuffer();
    String line = null;
    try {
      request.setCharacterEncoding("UTF-8");
      BufferedReader reader = request.getReader();
      while ((line = reader.readLine()) != null)
        jb.append(line);
    } catch (Exception localException1) {
    }
    try {
      System.out.println("SignUp");
      InitialContext ctx2 = new InitialContext();
      UserTransaction utx = (UserTransaction)ctx2.lookup("java:module/UserTransaction");
      utx.begin();
      request.setCharacterEncoding("UTF-8");
      String jsonStr = jb.toString();
      JSONObject data = new JSONObject(jsonStr);
      UserDAO userDAO = new UserDAO();
      List<UserDTO> users = userDAO.findByUsernamePassword(data.getString("name"), data.getString("email"));
      System.out.println(data.getString("name"));
      JSONObject jo = new JSONObject();
      if (users.size() != 0) {
        jo.put("response", "problem");
        UserDTO user = users.get(0);
        
        user.setLastvisitDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        user = (UserDTO)userDAO.makePersistent(user);
        UsergroupUserDAO uuDAO = new UsergroupUserDAO();
        UserGroupUserDTO uu = new UserGroupUserDTO();
        uu.setUser_id(user.getId());
        uu.setGroup_id(Long.valueOf("14"));
        uu = (UserGroupUserDTO)uuDAO.makePersistent(uu);
        jo.put("response", "ok");
        jo.put("userID", user.getId());
      } else {
        UserDTO user = new UserDTO();
        user.setEmail(data.getString("email"));
        user.setRegisterDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        user.setLastvisitDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
        user.setName(data.getString("name"));
        user.setPassword("1234");
        user.setUsername(data.getString("name"));
        user.setUsertype("Registered");
        user = (UserDTO)userDAO.makePersistent(user);
        UsergroupUserDAO uuDAO = new UsergroupUserDAO();
        UserGroupUserDTO uu = new UserGroupUserDTO();
        uu.setUser_id(user.getId());
        uu.setGroup_id(Long.valueOf("14"));
        uu = (UserGroupUserDTO)uuDAO.makePersistent(uu);
        jo.put("response", "ok");
        jo.put("userID", user.getId());
      }
      utx.commit();
      response.setCharacterEncoding("UTF-8");
      response.getOutputStream().write(jo.toString().getBytes());
    }
    catch (Exception ex)
    {
      ex.printStackTrace();
    }
  }
}